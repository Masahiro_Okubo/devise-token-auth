# README


「devise token auth を使って簡単に早くAPIを作る」シリーズの完成版APIです。
詳しい内容は以下のリンクよりご確認いただけると幸いです。

最近はなかなか時間が取れないので、エラー対応が遅れてしまうかもしれませんが、御了承下さい。

#### http://clc.gonna.jp/?p=1306


### Version
ruby 2.3.1

rails 5系

### Database creation
$ rake db:create
  
$ rake db:migrate

### How to run?
$ rails s

