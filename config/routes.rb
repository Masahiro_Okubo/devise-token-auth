Rails.application.routes.draw do


  resources :hobbies
  namespace :api do
    mount_devise_token_auth_for 'User', at: 'auth', controllers: {
        registrations: 'api/auth/registrations'
    }
    resources :books
  end

end
